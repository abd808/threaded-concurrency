### Overview

Codes were made in C , and aimed to meet the requirements specified in the Specs PDF.

**Aim**:

    - Understanding and implementing the working of pThreads in C.
    
    - Using mutex locks and sempahores as problem solving tools
    
**Structure**

    - Each question in the specs doc has its own C file as an answer.
    
    - Each answer has an associated ReadMe which details the implementation and the logic behind the solution.