#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
typedef struct student
{
    int waiting;   //whether he's currently waiting to be vaccinated, 1 for yes, 0 for no
    int turn; //which vaccine round he's on
    int ready;//antibodies present 0 for no, 1 for yes
    int cur_zone; //zone to which student is allotted
    float cur_prob; //probability in current zone
}student;

typedef struct batch
{ 
    int company;
    int va_count; // number of vaccines in each batch
}
batch;

typedef struct zone
{
    int waiting;
    int cur_company;
    int cur_batch; 
}zone;
// typedef struct cond
// {
//     pthread_cond_t cond;
// }
// cond;


struct batch* batches;   // holds all the batches produced and waiting to be assigned to a vaccination centre
struct student* students;// details of each student
struct zone* zones; // details of each zone

// struct cond* production_lock; // stops production of vaccines for each company until its batches become 0
// struct cond* student_wait;//stops students while they are alotted a zone
// struct cond* zone_wait;//stops a zone while its current phase is underway

int* company_vaccines;//counts number of batches in each company
//int* vaccination_phase;//counts number of people being vaccinated in each zone;
int n,m,o;  // input variables
float* prob; // contains probabilities for vaccines of each company
int tot_students;//students left to get vaccinated
int students_waiting; //students in line to get vaccinated
int yeet;

pthread_mutex_t batch_update;  //mutex controlling update of batches
pthread_mutex_t student_update; // controls updating assignment
pthread_mutex_t zone_update;// controls zone getting students

// pthread_cond_t zone_get_vaccine; // controls access to batches of vaccine if there are none
// pthread_cond_t zone_get_students; // controls access to allotment of students
// pthread_mutexattr_t Attr;


int printRandoms(int lower, int upper)
{ 
    int num = (rand() % 
           (upper - lower + 1)) + lower; 
    return num;
} 

int min(int num1, int num2) 
{
    return (num1 > num2 ) ? num2 : num1;
}

void* student_thread_func(void* inp)
{   
    const int id=(long)inp;

    while(students[id].turn<=3 && students[id].ready==0)
    {
        printf("Student %d has arrived for vaccination round number:%d\n\n",id,students[id].turn);
        printf("Student %d is waiting to be alotted a vaccination zone\n\n",id);

        pthread_mutex_lock(&student_update);
        students[id].waiting=1;
        students_waiting++;
        pthread_mutex_unlock(&student_update);

        while(students[id].waiting==1)
        {
            if(yeet)
                return NULL;
        }

        printf("Student %d on Vaccination Zone %d has been vaccinated which has success probability %f\n\n",id,students[id].cur_zone,students[id].cur_prob );
        sleep(3);
        if(students[id].ready==0){
            students[id].turn++;
            printf("Student %d has tested negative for antibodies\n\n",id);

            if(students[id].turn==4){
            printf("Student %d has been vaccinated 3 times and failed,and is now being yeeted back home\n\n",id);
            tot_students--;
            }
        }
        else
        {
             printf("Student %d has tested positive for antibodies\n\n",id);
             tot_students--;
        }       
    }
    if(tot_students==0)
         yeet=1;
    return NULL;
}

void* company_thread_func(void* inp)
{
    const int id=(long)inp;
    int batch_pointer=id*o; //location in batches array where particular company starts from
   
    while(tot_students!=0)
    {
        int batch_count;// counts batch_no while assigning
        int waiting_time=printRandoms(2,5);
        int no_of_batches=printRandoms(1,5);
        printf("Pharmaceutical Company %d is preparing %d batches of vaccines which have success probability %f\n\n",id,no_of_batches,prob[id]);
        sleep(waiting_time);
        int i=batch_pointer;
        batch_count=batch_pointer;

        batch_pointer+=no_of_batches; //total batches updated

        for( ;i<batch_pointer;i++)
        {
            batches[i].company=id;
            batches[i].va_count=printRandoms(10,20);
            company_vaccines[id]+=batches[i].va_count;
           // pthread_cond_signal(&zone_get_vaccine);
        }
        printf("Pharmaceutical Company %d has prepared %d batches of vaccines which have success probability %f.\nWaiting for all the vaccines to be used to resume production\n\n",id,no_of_batches,prob[id]);

        i=0;
        while(no_of_batches!=0)
        {
            pthread_mutex_lock(&batch_update);
            if(zones[i].waiting==1)
            {
                zones[i].waiting=0;
                zones[i].cur_company=id;
                zones[i].cur_batch=batch_count;
                batch_count++;
                no_of_batches--;
                printf("Pharmaceutical Company %d is supplying a batch to Vaccination Zone %d with probability %f\n\n",id,i,prob[id]);
            }
            if(i==m-1)
                i=0;
            else
                i++;            
            pthread_mutex_unlock(&batch_update);
        }

        while(company_vaccines[id]!=0)
        {
            if(yeet) return NULL;
        }

        printf("Pharma Company %d has finished all its vaccines and is now resuming production\n\n",id);
        sleep(3);
    }

    return NULL;
}

void* zone_thread_func(void *inp)
{
    const int id=(long)inp;
    
    while(tot_students!=0)
    {   
        printf("Vaccination Zone %d waiting for vaccines to become available\n\n",id);
        while (zones[id].waiting==1)
        {
            if(yeet)
                return NULL;
        }  

        printf("Pharmaceutical Company %d has delivered %d vaccines to  Vaccination zone %d, resuming vaccinations now\n\n",zones[id].cur_company,batches[zones[id].cur_batch].va_count,id);        
        sleep(2);
        int b_no=zones[id].cur_batch;
        int vaccines=batches[b_no].va_count;
        int to_be_succesful=(int)(batches[b_no].va_count*prob[batches[b_no].company]);
        int to_be_unsucessful=batches[b_no].va_count-to_be_succesful;
        while(vaccines)
        {
            pthread_mutex_lock(&zone_update);
            int upper_limit=min(8,min(students_waiting,vaccines));
            int slots=printRandoms(1,upper_limit);
            int students_alotted[slots];
            int students_alotted_count=0;
            company_vaccines[zones[id].cur_company]-=slots;
            vaccines-=slots;
            printf("Vaccination Zone %d is ready to vaccinate with %d slots​\n\n",id,slots);
            sleep(2);
            for(int i=0;i<o && slots>0;i++)
            {
                if(students[i].waiting==1)
                {
                    students[i].cur_prob=prob[zones[id].cur_company];
                    students[i].cur_zone=id;
                    students_alotted[students_alotted_count++]=i;
                    printf("Student %d assigned a slot on the Vaccination Zone %d and waiting to be vaccinated\n\n",i,students[i].cur_zone);
                    slots--;
                }
            }
            pthread_mutex_unlock(&zone_update);
            sleep(3);
            printf("Vaccination Zone %d entering Vaccination Phase\n\n",id);
            sleep(2);
            for (int i=0;i<students_alotted_count;i++)
            {
                if(to_be_succesful==0)
                    students[i].ready=0;
                else if(to_be_unsucessful==0)
                    students[i].ready=1;
                else
                {
                    int decide=printRandoms(0,1);
                    if(decide==0)
                        to_be_unsucessful--;
                    else if(decide==1)
                        to_be_succesful--;
                    students[i].ready=decide;
                }
                
                students[students_alotted[i]].waiting=0;
            }
        }
        sleep(3);
        zones[id].waiting=1;
        printf("Vaccination Zone %d has run out of Vaccines\n\n",id);
    }
    return NULL;
}

int main()
{
    // pthread_mutexattr_init(&Attr);
    // pthread_mutexattr_settype(&Attr, PTHREAD_MUTEX_RECURSIVE);

    yeet=0;
    pthread_mutex_init(&batch_update, NULL);
    pthread_mutex_init(&student_update,NULL);
    pthread_mutex_init(&zone_update,NULL);
    //pthread_cond_init(&zone_get_vaccine,NULL);

    scanf("%d %d %d",&n,&m,&o);

    tot_students=o;// initally all students need to get vaccinated
    students_waiting=0;//No student arrived yet

    company_vaccines=(int*)malloc(n*sizeof(int));    
    //vaccination_phase=(int*)malloc(m*sizeof(int));
    students=(student*)malloc(o*sizeof(student));
    batches=(batch*)malloc((n*20*o)*sizeof(batches));
    zones=(zone*)malloc(m*sizeof(zone));
    prob=(float*)malloc(n*sizeof(float));

    if(n==0)
    {
        printf("No companies present and no vaccines get produced\n\n");
        printf("SIMULATION OVER !!!\n\n");        
        exit(0);
    }

    if(m==0)
    {
        printf("No Vaccination Zones present\n\n");
        printf("SIMULATION OVER !!!\n\n");
        exit(0);
    }

    if(o==0)
    {
        printf("No students to be vaccinated\n\n");
        printf("SIMULATION OVER !!!\n\n");;
        exit(0);
    }

    // production_lock=(cond*)malloc(n*sizeof(cond));
    // student_wait=(cond*)malloc(o*sizeof(cond));
    // zone_wait=(cond*)malloc(m*sizeof(cond));
    pthread_t student_thread[o];
    pthread_t company_thread[n];
    pthread_t zone_thread[m];

    for(int i=0;i<n;i++)
        scanf("%f",&prob[i]);
    

    for (int i=0;i<o;i++)
    {
        students[i].ready=0;
        students[i].turn=1;
        students[i].waiting=0;
        //pthread_cond_init(&student_wait[i].cond,NULL);
        pthread_create(&student_thread[i],NULL,student_thread_func,(void*)(long)i);
    }

    for (int i=0;i<n;i++)
    {
       // pthread_cond_init(&production_lock[i].cond,NULL);
        company_vaccines[i]=0; // no company has any batches rn
        pthread_create(&company_thread[i],NULL,company_thread_func,(void*)(long)i);
    }

    for (int i=0;i<m;i++)
    {
        zones[i].waiting=1;
       // pthread_cond_init(&zone_wait[i].cond,NULL);
        pthread_create(&zone_thread[i],NULL,zone_thread_func,(void*)(long)i);
    }

    for (int i=0;i<o;i++)
    {
        pthread_join(student_thread[i],NULL);
    }
    
    // for (int i=0;i<n;i++)
    // {
    //     pthread_join(company_thread[i],NULL);
    // }

    // for (int i=0;i<m;i++)
    // {
    //     pthread_join(zone_thread[i],NULL);
    // }

    printf("SIMULATION OVER !!!\n\n");
    exit(0);
}