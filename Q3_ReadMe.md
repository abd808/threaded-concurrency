	* dual musicians is used to refer to musicians who play both a and e stages.
	* the bonus specs have been met, the singers collect t-shirts, and each print statement provides information as to the stage number on which each performer performs. 
	*NOTE* The stages are numbered 0 to e-1 for electric, and 0 to a-1 for acoustic.

### Semaphores used:
1. value `a`, to control the access to solo performances on acoustic stages

2. value `e`, to control the access to solo performances on electric stages
3. value `a`, and additional semaphore to contol the number of singers accesssing acoustic stages, (as each stage can always hold 	one singer, either as a solo performer, or a duet)
4. value `e`. additional singer semaphore for electric stages, similar to number *3*.
5. a binary semaphore for each musician who can perform on both stages, which signifies successful allotment of a stage.
6. a binary semaphore similar to number *5* , but for each singer, signifying successful allotment of stage.
7. a binary semaphore for each singer, which signifies successful completion of a duet (in case a duet is undertaken).
8. value e, semaphore contoliing number of free co-ordinators to supply t-shirts.

### Mutex used:

1. for allotting and freeing up acoustic stages, ensuring atomicity.

2. for allotting and freeing up electric stages, ensuring atomicity.


 ### Data Structures used


1. an array of structures, holding a semaphore, used to create binary semaphores
2. an array of structures, holding details of each musician
3. an array of structures, holding details for each singer
4. an array of structures, holding details of each stage- one array each for acoustic and elecric stage

### Functions:

1. `main`:
 - takes input of system variables, and details of each arriving musician/singer 
-  separates the musicians from the singers for purpose of future functions 
- stores a separate list of musicians who play g and p.
- creates threads for each entity , calls their respective functions and waits on the threads to join.

2. `musician_thread_func` : waits the specified amount of time for each musician to arrive,
	Called by each musican thread , it holds three cases:
	1. for either 'v' or 'b' musicians , it enters respective semaphore, or waits on it if required, and thus adding a lock on to the semaphore on entering, this waiting done by _trywait_ exits the function on passing of t seconds. It enters the allocation mutex lock on successful passing of semaphore, occupies a stage, and performs for a random interval of time. It then checks whether a singer has joined , using a bool value variable , and if affirmative ,extends the performance by 2 seconds, and signals a post operation on the binary wait semaphore of the associated singer.
	2. for 'g' and 'p' musicians, it creates a further two threads, each which wait on acoustic and electric stages respectively, on return from both the threads, checks a bool variable for whether the musician has performed, and leaves due to imatience if value is 0.
	3. has a semaphore entry point for shirt co-ordinators, with value c, fairly simple, waits on semaphore and supplies a t-shirt to all the musicians. Additionally has a check for case where c=0.
	
3. `dual_musician_thread`: *functions*: two functions , for each dual musician, one that waits on acoustic stages semaphore, and another that waits on electric stages semaphore. Each wait is done using timedwait and exits after t seconds. On successful locking of semaphore, it locks the binary semaphore associated with the musician, and hence the function that locks its stage semaphore second (in case it happens) will not be able to lock the binary success semaphore, and operates a post operation on its stage semaphore, freeing up a spot and returns to the calling thread.
	The thread that is successful does the exact same operations as those done for a 'v' or 'b' musician, updates the bool variable that checks whether a musician has successfully performed , and returns to the calling thread.
	
4. `singer_thread_func`: Waits for the singer to arrive,and creates two threads , like the dual musician, and performs the same waiting and checking , and has the c semaphore that controls allotment of t-shirts .

5. `singer_electric/acoustic_thread_funcs`: similar to dual musicians , they performed timed wait on two different semaphores each, and update the binary success semaphore associated with each musician on successful locking.
	- The semaphores waited on initially are the 'a' and 'e' valued semaphores that are exclusive to singers.

			The next part is the same for both funcs, with either acoustic or electric stages being allotted according to the function

	- On successful locking, it creates an array of all available stages, and chooses a random stage from this array, hence it does not matter if the stage is being currently used by a solo performer or not.

	- If the stage is currently being used by a solo musician , it updates a bool variable in the stage structure, and performs lock operation on its binary wait semaphore, that is associated with each singer,by calling wait twice consecutively on it, and hence waits for a signal for when the duet ends, after which it performs a post operation on the exclusive singers semaphore.

	- If the stage is currently empty, it blocks the stage from being allotted to another solo or singer, calls wait on the solo_stage semaphore and performs a normal solo performance for a random interval, after which it calls post operation on both the solo semaphore and the exclusive singers semaphore , updates the bool variable for successful performace, and returns to the calling thread

	- The allotment of stages is done under a mutex lock for the respective stage type.
	
On execution of all the above threads, the program exits. 

	
