#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <pthread.h> 
#include <semaphore.h> 
#include <unistd.h> 
#include <time.h>
#include <errno.h>

void red () {
  printf("\033[1;31m");
}

void arrived(){
    printf("\033[1;32m");
}

void acoustic(){
    printf("\033[1;34m");
}

void t_shirt(){
    printf("\033[1;35m");
}

void impatience(){
    printf("\033[1;31m");
}

void electric(){
    printf("\033[1;33m");
}

void singer_color(){
    printf("\033[1;36m");
}

typedef struct stage
{
    char cur_performer[40];
    int solo_in_progress;
    int duet_in_progress;
    int singer_id;
} stage;

typedef struct musician
{
    char name[40];
    char instrument;
    int eta;
    int dual_id; // used to maintain an ordering of musicians who play both a and e stages.
} musician;

typedef struct singer
{
    char name[40];
    int eta;
} singer;

typedef struct semaphore
{
    sem_t sem;
} semaphore;

sem_t solo_acoustic;
sem_t solo_electric;
sem_t co_ordinators;
sem_t singers_acoustic;
sem_t singers_electric;

pthread_mutex_t allocate_solo_electric;
pthread_mutex_t allocate_solo_acoustic;

struct stage* acoustic_stages;// acoustic stage details. Acts as a resource bank
struct stage* electric_stages;// simiilarly stores details
struct musician* musicians;// details of each musician
struct singer* singers; // details of all the singers;
struct semaphore* success_dual;// a semaphore that checks whether a dua musician has successfully been alotted a stage
struct semaphore* success_singer; // similar to dual musician, bimary semaphore
struct semaphore* wait_singer; // waiting for its performance to end while dueting

int no_of_singers;
int no_of_musicians;
int dual_id_count; // counts number of dual musicians
int* dual_has_performed;//checks whether dual performance has occured
int* singer_has_performed;// checks whether singer has performed

int a,e,c,t,k,t1,t2; // input variables

int printRandoms(int lower, int upper)
{ 
    int num = (rand() % 
           (upper - lower + 1)) + lower; 
    return num;
} 

int min(int num1, int num2) 
{
    return (num1 > num2 ) ? num2 : num1;
}

void* dual_acoustic_func(void* inp)
{
    const int id=(long)inp;
    int dual=musicians[id].dual_id;
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts.tv_sec += t;
    int s=sem_timedwait(&solo_acoustic,&ts);      //waiting to be given access to an acoustic stage
    if(s==-1)
        return NULL;                         

    struct timespec ts1;
    if (clock_gettime(CLOCK_REALTIME, &ts1) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts1.tv_sec+=0;
    int s2=sem_timedwait(&success_dual[dual].sem,&ts1);  //checks if musician has already been alotted an electric stage 
    if(s2==-1)
    {
        sem_post(&solo_acoustic);
        return NULL;
    }

    int stage_no;
    pthread_mutex_lock(&allocate_solo_acoustic); // making sure no clashes in allocation of stage
    for (int i=0;i<a;i++)
    {
        if(acoustic_stages[i].solo_in_progress==0)
        {
            stage_no=i;
            strcpy(acoustic_stages[i].cur_performer,musicians[id].name);
            acoustic_stages[i].solo_in_progress=1;
            break;
        }
    }
    pthread_mutex_unlock(&allocate_solo_acoustic);
    int performance_duration=printRandoms(t1,t2);
    acoustic();
    printf("%s performing %c on acoustic stage no:%d for %d sec\n\n",musicians[id].name,musicians[id].instrument,stage_no,performance_duration);
    sleep(performance_duration);

    if(acoustic_stages[stage_no].duet_in_progress==1)
    {
        singer_color();
        printf("%s joined %s's performance, and extended it by 2 seconds\n\n",acoustic_stages[stage_no].cur_performer,musicians[id].name);
        sleep(2);
        sem_post(&wait_singer[acoustic_stages[stage_no].singer_id].sem);
    }

    pthread_mutex_lock(&allocate_solo_acoustic);
    acoustic_stages[stage_no].solo_in_progress=0;
    acoustic_stages[stage_no].duet_in_progress=0;
    pthread_mutex_unlock(&allocate_solo_acoustic);

    acoustic();
    printf("%s performance at acoustic stage no:%d ended\n\n",musicians[id].name,stage_no);

    dual_has_performed[dual]=1;
    sem_post(&solo_acoustic);
    return NULL;
    
}

void* dual_electric_func(void* inp)
{
    const int id=(long)inp;
    int dual=musicians[id].dual_id;
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts.tv_sec += t;
    int s=sem_timedwait(&solo_electric,&ts);      //waiting to be given access to an acoustic stage
    if(s==-1)
        return NULL;                         

    struct timespec ts1;
    if (clock_gettime(CLOCK_REALTIME, &ts1) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts1.tv_sec+=0;
    int s2=sem_timedwait(&success_dual[dual].sem,&ts1);  //checks if musician has already been alotted an electric stage 
    if(s2==-1)
    {
        sem_post(&solo_electric);
        return NULL;
    }

    int stage_no;
    pthread_mutex_lock(&allocate_solo_electric); // making sure no clashes in allocation of stage
    for (int i=0;i<e;i++)
    {
        if(electric_stages[i].solo_in_progress==0)
        {
            stage_no=i;
            strcpy(electric_stages[i].cur_performer,musicians[id].name);
            electric_stages[i].solo_in_progress=1;
            break;
        }
    }
    pthread_mutex_unlock(&allocate_solo_electric);
    int performance_duration=printRandoms(t1,t2);
    electric();
    printf("%s performing %c on electric stage no:%d for %d sec\n\n",musicians[id].name,musicians[id].instrument,stage_no,performance_duration);
    sleep(performance_duration);


    if(electric_stages[stage_no].duet_in_progress==1)
    {
        singer_color();
        printf("%s joined %s's performance, and extended it by 2 secs\n\n",electric_stages[stage_no].cur_performer,musicians[id].name);
        sleep(2);
        sem_post(&wait_singer[electric_stages[stage_no].singer_id].sem);
    }

    pthread_mutex_lock(&allocate_solo_electric);
    electric_stages[stage_no].solo_in_progress=0;
    electric_stages[stage_no].duet_in_progress=0;
    pthread_mutex_unlock(&allocate_solo_electric);

    electric();
    printf("%s performance at electric stage no:%d ended\n\n",musicians[id].name,stage_no);

    sem_post(&solo_electric);
    dual_has_performed[dual]=1;
    return NULL;
}

void* musician_thread_func(void* inp)
{
    const int id=(long)inp;
    sleep(musicians[id].eta);
    arrived();
    printf("%s arrived at Srujana to play %c\n\n",musicians[id].name,musicians[id].instrument);
    struct timespec ts;

    if(musicians[id].instrument=='b')
    {
        
               if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
                   perror("clock_gettime");
                   exit(0);
               }
               ts.tv_sec += t;
        int s;    
        s=sem_timedwait(&solo_electric,&ts);
        if(s==-1)
        {
            impatience();
            printf("%s leaving due to impatience\n\n",musicians[id].name);
            return NULL;
        }
        else
        {
            int stage_no;
            pthread_mutex_lock(&allocate_solo_electric); // making sure no clashes in allocation of stage
            for (int i=0;i<e;i++)
            {
                if(electric_stages[i].solo_in_progress==0)
                {
                    stage_no=i;
                    strcpy(electric_stages[i].cur_performer,musicians[id].name);
                    electric_stages[i].solo_in_progress=1;
                    break;
                }
            }
            pthread_mutex_unlock(&allocate_solo_electric);
            int performance_duration=printRandoms(t1,t2);
            electric();
            printf("%s performing %c on electric stage no:%d for %d sec\n\n",musicians[id].name,musicians[id].instrument,stage_no,performance_duration);
            sleep(performance_duration);

            if(electric_stages[stage_no].duet_in_progress==1)
            {
                singer_color();
                printf("%s joined %s's performance, and extended it by 2 secs\n\n",electric_stages[stage_no].cur_performer,musicians[id].name);
                sleep(2);
                sem_post(&wait_singer[electric_stages[stage_no].singer_id].sem);
            }

            pthread_mutex_lock(&allocate_solo_electric);
            electric_stages[stage_no].solo_in_progress=0;
            electric_stages[stage_no].duet_in_progress=0;
            pthread_mutex_unlock(&allocate_solo_electric);

            electric();
            printf("%s performance at electric stage no:%d ended\n\n",musicians[id].name,stage_no);

            sem_post(&solo_electric);
        }        
    }

    else if(musicians[id].instrument=='v')
    {
        
               if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
                   perror("clock_gettime");
                   exit(0);
               }
               ts.tv_sec += t;
        int s;    
        s=sem_timedwait(&solo_acoustic,&ts);
        if(s==-1)
        {
            impatience();
            printf("%s leaving due to impatience\n\n",musicians[id].name);
            return NULL;
        }
        else
        {
            int stage_no;
            pthread_mutex_lock(&allocate_solo_acoustic); // making sure no clashes in allocation of stage
            for (int i=0;i<a;i++)
            {
                if(acoustic_stages[i].solo_in_progress==0)
                {
                    stage_no=i;
                    strcpy(acoustic_stages[i].cur_performer,musicians[id].name);
                    acoustic_stages[i].solo_in_progress=1;
                    break;
                }
            }
            pthread_mutex_unlock(&allocate_solo_acoustic);
            int performance_duration=printRandoms(t1,t2);
            acoustic();
            printf("%s performing %c on acoustic stage no:%d for %d sec\n\n",musicians[id].name,musicians[id].instrument,stage_no,performance_duration);
            sleep(performance_duration);

            if(acoustic_stages[stage_no].duet_in_progress==1)
            {
                singer_color();
                printf("%s joined %s's performance, and extended it by 2 seconds\n\n",acoustic_stages[stage_no].cur_performer,musicians[id].name);
                sleep(2);
                sem_post(&wait_singer[acoustic_stages[stage_no].singer_id].sem);
            }

            pthread_mutex_lock(&allocate_solo_acoustic);
            acoustic_stages[stage_no].solo_in_progress=0;
            acoustic_stages[stage_no].duet_in_progress=0;
            pthread_mutex_unlock(&allocate_solo_acoustic);

            acoustic();
            printf("%s performance at acoustic stage no:%d ended\n\n",musicians[id].name,stage_no);

            sem_post(&solo_acoustic);
        }        
    }

    else
    {
        pthread_t electric;
        pthread_t acoustic;
        sem_init(&success_dual[musicians[id].dual_id].sem,0,1);

        pthread_create(&electric,NULL,dual_electric_func,(void*)(long)id);
        pthread_create(&acoustic,NULL,dual_acoustic_func,(void*)(long)id);

        pthread_join(electric,NULL);
        pthread_join(acoustic,NULL);       



        if(dual_has_performed[musicians[id].dual_id]==0){
            sem_destroy(&success_dual[musicians[id].dual_id].sem);
            impatience();
            printf("%s leaving due to impatience\n\n",musicians[id].name);
            return NULL;
        }

        sem_destroy(&success_dual[musicians[id].dual_id].sem); 

    }
    

    if (c!=0)
    {
        
        sem_wait(&co_ordinators);
        t_shirt();
        printf("%s collecting t-shirt\n\n",musicians[id].name);
        sleep(2);
        sem_post(&co_ordinators);

    }
    else
    {
        red();
        printf("The co-ordinators dgaf, no t-shirts \n\n");
    }
    
}

void* singers_acoustic_thread_func(void* inp)
{
    const int id=(long) inp;
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts.tv_sec += t;
    int s=sem_timedwait(&singers_acoustic,&ts);      //waiting to be given access to an acoustic stage
    if(s==-1)
        return NULL;   

    struct timespec ts1;
    if (clock_gettime(CLOCK_REALTIME, &ts1) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts1.tv_sec+=0;
    int s2=sem_timedwait(&success_singer[id].sem,&ts1);  //checks if musician has already been alotted an electric stage 
    if(s2==-1)
    {
        sem_post(&singers_acoustic);
        return NULL;
    }   
    int duet_solo=0;// 0 for duet, 1 for solo

    pthread_mutex_lock(&allocate_solo_acoustic);
    int arr[a];
    int available_count=0;// array and count to store which all stages are available
    for(int i=0;i<a;i++)
    {
        if(acoustic_stages[i].duet_in_progress==0)
        arr[available_count++]=i;
    }
    int chosen=printRandoms(0,available_count-1);
    chosen=arr[chosen];
    acoustic_stages[chosen].duet_in_progress=1;
    if(acoustic_stages[chosen].solo_in_progress==0)
    {
        acoustic_stages[chosen].solo_in_progress=1;
        duet_solo=1;
        sem_wait(&solo_acoustic);
    }
    pthread_mutex_unlock(&allocate_solo_acoustic);

    if(duet_solo==1)
    {
        int performance_time=printRandoms(t1,t2);
        singer_color();
        printf("Singer %s is performing solo on acoustic stage no:%d for %d sec\n\n",singers[id].name,chosen,performance_time);
        sleep(performance_time);

        pthread_mutex_lock(&allocate_solo_acoustic);
        acoustic_stages[chosen].solo_in_progress=0;
        acoustic_stages[chosen].duet_in_progress=0;
        pthread_mutex_unlock(&allocate_solo_acoustic);
        singer_has_performed[id]=1;

        singer_color();
        printf("%s solo at acoustic stage no:%d ended\n\n",singers[id].name,chosen);
        sem_post(&singers_acoustic);
        sem_post(&solo_acoustic);
        return NULL;
    }
    else
    {
        strcpy(acoustic_stages[chosen].cur_performer,singers[id].name);
        acoustic_stages[chosen].singer_id=id;
        sem_init(&wait_singer[id].sem,0,1);
        sem_wait(&wait_singer[id].sem);
        sem_wait(&wait_singer[id].sem);
        singer_color();
        printf("%s duet on acoustic stage no:%d ended\n\n",singers[id].name,chosen);
        singer_has_performed[id]=1;
        sem_post(&singers_acoustic);
        return NULL;
    }

   return NULL;
}

void* singers_electric_thread_func(void* inp)
{
    const int id=(long) inp;
    struct timespec ts;
    if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts.tv_sec += t;
    int s=sem_timedwait(&singers_electric,&ts);      //waiting to be given access to an acoustic stage
    if(s==-1)
        return NULL;   

    struct timespec ts1;
    if (clock_gettime(CLOCK_REALTIME, &ts1) == -1) {
    perror("clock_gettime");
    exit(0);
    }
    ts1.tv_sec+=0;
    int s2=sem_timedwait(&success_singer[id].sem,&ts1);  //checks if musician has already been alotted an electric stage 
    if(s2==-1)
    {
        sem_post(&singers_electric);
        return NULL;
    }

    int duet_solo=0;// 0 for duet, 1 for solo

    pthread_mutex_lock(&allocate_solo_electric);
    int arr[e];
    int available_count=0;// array and count to store which all stages are available
    for(int i=0;i<e;i++)
    {
        if(electric_stages[i].duet_in_progress==0)
        arr[available_count++]=i;
    }
    int chosen=printRandoms(0,available_count-1);
    chosen=arr[chosen];
    electric_stages[chosen].duet_in_progress=1;
    if(electric_stages[chosen].solo_in_progress==0)
    {
        electric_stages[chosen].solo_in_progress=1;
        duet_solo=1;
        sem_wait(&solo_electric);
    }
    pthread_mutex_unlock(&allocate_solo_electric);

    if(duet_solo==1)
    {
        int performance_time=printRandoms(t1,t2);
        singer_color();
        printf("Singer %s is performing solo on electric stage no:%d for %d sec\n\n",singers[id].name,chosen,performance_time);
        sleep(performance_time);

        pthread_mutex_lock(&allocate_solo_electric);
        electric_stages[chosen].solo_in_progress=0;
        electric_stages[chosen].duet_in_progress=0;
        pthread_mutex_unlock(&allocate_solo_electric);
        singer_has_performed[id]=1;

        singer_color();
        printf("%s solo at electric stage no:%d ended\n\n",singers[id].name,chosen);
        sem_post(&singers_electric);
        sem_post(&solo_electric);
        return NULL;
    }
    else
    {
        strcpy(electric_stages[chosen].cur_performer,singers[id].name);
        electric_stages[chosen].singer_id=id;
        sem_init(&wait_singer[id].sem,0,1);
        sem_wait(&wait_singer[id].sem);
        sem_wait(&wait_singer[id].sem);
        singer_color();
        printf("%s duet electric stage no:%d ended\n\n",singers[id].name,chosen);
        singer_has_performed[id]=1;
        sem_post(&singers_electric);
        return NULL;
    }

   return NULL;
}

void* singer_thread_func(void* inp)
{
    const int id=(long)inp;
    sleep(singers[id].eta);
    arrived();
    printf("%s has arrived at Srujana to sing\n\n",singers[id].name);

    sem_init(&success_singer[id].sem,0,1);
    pthread_t electric;
    pthread_t acoustic;

    pthread_create(&electric,NULL,singers_electric_thread_func,(void*)(long)id);
    pthread_create(&acoustic,NULL,singers_acoustic_thread_func,(void*)(long)id);

    pthread_join(electric,NULL);
    pthread_join(acoustic,NULL);

    if(singer_has_performed[id]==0)
    {
        sem_destroy(&success_singer[id].sem);
        impatience();
        printf("%s leaving due to impatience\n\n",singers[id].name);
        return NULL;
    }

    if (c!=0)
    {
        
        sem_wait(&co_ordinators);
        t_shirt();
        printf("%s collecting t-shirt\n\n",singers[id].name);
        sleep(2);
        sem_post(&co_ordinators);

    }
    else
    {
        red();
        printf("The co-ordinators dgaf, no t-shirts \n\n");
    }
    sem_destroy(&success_singer[id].sem);

    return NULL;
}

int main()
{
    no_of_singers=0;
    no_of_musicians=0;
    dual_id_count=0;
    
    printf("Enter System Variables\n");
    scanf("%d %d %d %d %d %d %d",&k,&a,&e,&c,&t1,&t2,&t);

    sem_init(&solo_acoustic,0,a);
    sem_init(&solo_electric,0,e);
    sem_init(&co_ordinators,0,c);
    sem_init(&singers_acoustic,0,a);
    sem_init(&singers_electric,0,e);

    pthread_mutex_init(&allocate_solo_acoustic,NULL);
    pthread_mutex_init(&allocate_solo_electric,NULL);

    //setting the max number of each type of performance that can occur simultaneouesly
    
    acoustic_stages=(stage*)malloc(a*sizeof(stage));
    electric_stages=(stage*)malloc(e*sizeof(stage));
    musicians=(musician*)malloc(k*sizeof(musician));
    singers=(singer*)malloc(k*sizeof(singer));

    for(int i=0;i<k;i++)
    {
        printf("\nEnter Details for Person:%d\n",i);
        char name[40];
        char ch;
        int time;
        scanf("%s",name);
        scanf(" %c",&ch);
        scanf("%d",&time);
        if(ch=='s')
        {
           // printf("entering singer input");
            singers[no_of_singers].eta=time;
            strcpy(singers[no_of_singers].name,name);
            no_of_singers++;
        }
        else
        {
            musicians[no_of_musicians].eta=time;
            musicians[no_of_musicians].instrument=ch;
            strcpy(musicians[no_of_musicians].name,name);
            no_of_musicians++;
        }
        if(ch=='g' || ch=='p')
        {
            musicians[no_of_musicians-1].dual_id=dual_id_count++;
        }
    }

    pthread_t musicians_thread[no_of_musicians];
    pthread_t singers_thread[no_of_singers];

    success_dual=(semaphore*)malloc(dual_id_count*sizeof(semaphore));
    dual_has_performed=(int*)malloc(dual_id_count*sizeof(int));

    success_singer=(semaphore*)malloc(no_of_singers*sizeof(semaphore));
    singer_has_performed=(int*)malloc(no_of_singers*sizeof(int));
    wait_singer=(semaphore*)malloc(no_of_singers*sizeof(semaphore));

    printf("\n");


    for(int i=0;i<no_of_musicians;i++)
        pthread_create(&musicians_thread[i],NULL,musician_thread_func,(void*)(long)i);
    for(int i=0;i<no_of_singers;i++)
        pthread_create(&singers_thread[i],NULL,singer_thread_func,(void*)(long)i);


    for(int i=0;i<no_of_musicians;i++)
        pthread_join(musicians_thread[i],NULL);
    for(int i=0;i<no_of_singers;i++)
        pthread_join(singers_thread[i],NULL);
            
    red();
    printf("Exited\n\n");
}