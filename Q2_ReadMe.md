
Three mutex locks have been used for this implementation.

The three critical zones are:

1. For the arrival of students, so that no two students update the waiting list at the same time.
2. For allocation of a zone to a company so that two companies both don't supply the same zone with a batch
3. For allotment of zones to student , so that no student is alotted to two zones

**Implementation Details:**

- Data Structures:

1. Structs `student`,`zone`, and `batch` : Self describing and hold details for each student,zone and batch of vaccines created.

2. A deduction is made that with each student being vaccinated a maximum of 3 times, and each batch containing a minimum of 10 vaccines, the number of batches needed will definitely be lesser that *3o* where o is number of students, and hence definitely lesser than an arbitrary large number (n.20.o) where n is the number of companies.
	This is done to assign a division of the array of batch structures to each company where it can store its batches after  production and ensures that there is never any overlap of storage space simultaneously.
	
3. A probability array stores the success probability for vaccines produced by each company


- Functions:

1. `student`: On arrival of each student, increments the waiting list and shows itself available for allotment, and proceeds to wait. On allotment tests the student and decides whether to return to waiting or exit if tested positive.

		This was a part I wasn't sure about , whether testing if conditions where to be put in the vaccination zone , or student function, I went with the student function as it adds an element of concurrency , and each student is independent of the other while testing and this part is not included in the critical section
 
2. `zone`: Initially all zones wait to be supplied with a batch and a waiting variable is accordingly set to 1, on being supplied with a batch, it calculates how many students must be vaccinated successfully and otherwise, based on the probabilty of the company that supplies it, which is taken from its struct variable.

	- It decides a number of slots for each phase according to the specified criteria, and iterates over the waiting students and chooses them on a FIFO basis.

	- It exits the critical section of allotment so that other zones may continue, now with its list of students stored in an array, it assigns a random success or failure status to each student, until the number of successes calculated earlier, or failures, has been met, aftere which the result is decided to satisfy the remaining criteria.

	- It makes the respective changes in the student structure , regarding details of zone and probability, and then removes the student from waiting.
	
	- For each vaccine used, it updated a variable holding number of active vaccines of the supplying company so that it can be updated to 0 and the production may restart.

	- This process of searching for students continues till it runs out of vaccines it its batch, and then it returns to waiting.
	
3. `company`: Makes a random number of batches with each having a random number of vaccines according to assignment specs.
	
	- While it still has unsupplied batches, it loops over all the zones looking for one that is waiting, and on finding one, gives it a batch by updating zone struct, updates its batch count .

	- The part inside the loop is bounded by a mutex lock so that no two companies allot to the same zone.

	- When all its batches have been allotted, it waits for all its vaccines to be used , and then resumes production.

	- The produced batches are stored in the common *batches* array where each company has been assigned a start point
	
	
- General:

1. Each waiting loop has an if condition to check whether the simulation has ended so that it is never stuck indefinitely.
 
