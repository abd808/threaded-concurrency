
### Question 1 (q1.c)

- The following are output excerpts from the terminal comparing run times for different values of n

- The values have all been generated randomly and tested a few times, with resulting values not showing to much difference , a more rigorous way of testing would be to mark out average and variance for these values but the concept can still be grasped with this limited example as well.


        n= 400
        normal_mergeSort ran:
                [ 104.368784 ] times faster than concurrent_mergeSort
                [ 95.059809 ] times faster than threaded_concurrent_mergeSort

        n=100
        normal_mergeSort ran:
                [ 106.953243 ] times faster than concurrent_mergeSort
                [ 136.743397 ] times faster than threaded_concurrent_mergeSort
                        
        n=21	
        normal_mergeSort ran:
                [ 176.226028 ] times faster than concurrent_mergeSort
                [ 118.123812 ] times faster than threaded_concurrent_mergeSort

        n=7
        normal_mergeSort ran:
                [ 66.818600 ] times faster than concurrent_mergeSort
                [ 37.105051 ] times faster than threaded_concurrent_mergeSort




- 	The Normal merge sort performs significantly better than the concurrent Merge sort created using child processes 

-	The main reason for this is cache misses.
	Each time a child process needs to access a part of the array , for eg: left child accessing left half, those values need to be loaded onto the cache of the processor.
	As every process accesses a different part of the array, each process needs an overwriting of the cache.
	This to-and-fro process continues and it degrades the performance to such a level that it performs poorer than the sequential code.

- 	Similarly , the overhead caused by setting up and creation of threads is simply not worth it amd hence performs poorer in comparision to normal mergesort.
-	As the workload given to each thread is pretty small, the creation of threads does not help reduce the time taken through its use of concurrency , but instead creates overhead through the time taken up for creation.
- 	The creation of a large number of threads might not achieve true paralellization as the processor doesnt always have that many cores available.
-	If we designated a larger cut off point of work to be done by each thread, for large enough arrays, multi threaded solution would in theory be faster than the normal mergeSort.







